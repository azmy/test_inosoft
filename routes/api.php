<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\MobilController;
use App\Http\Controllers\Api\MotorController;
use App\Http\Controllers\Api\KendaraanController;
use App\Http\Controllers\Api\PenjualanController;
use App\Http\Controllers\Api\LaporanPenjualanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API route for register new user
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//API route for login user
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

//Route::group(['middleware' => ['auth:sanctum']], function () {
    //return $request->user();


// Route::middleware([Authenticate::class])->group(function () {
    // Definisi route yang memerlukan otentikasi JWT
  

Route::get('/mobils', [MobilController::class, 'index']);
Route::post('/mobils', [MobilController::class, 'store']);
Route::get('/mobils/{id}', [MobilController::class, 'show']);
Route::put('/mobils/{id}', [MobilController::class, 'update']);
Route::delete('/mobils/{id}', [MobilController::class, 'destroy']);

Route::get('/motors', [MotorController::class, 'index']);
Route::post('/motors', [MotorController::class, 'store']);
Route::get('/motors/{id}', [MotorController::class, 'show']);
Route::put('/motors/{id}', [MotorController::class, 'update']);
Route::delete('/motors/{id}', [MotorController::class, 'destroy']);

Route::get('/kendaraans', [KendaraanController::class, 'index']);
Route::post('/kendaraans', [KendaraanController::class, 'store']);
Route::get('/kendaraans/{id}', [KendaraanController::class, 'show']);
Route::put('/kendaraans/{id}', [KendaraanController::class, 'update']);
Route::delete('/kendaraans/{id}', [KendaraanController::class, 'destroy']);

Route::get('/penjualans',[PenjualanController::class,'index']);
Route::post('/penjualans',[PenjualanController::class,'store']);
Route::get('/laporan-penjualan-perkendaraans/{id}',[LaporanPenjualanController::class,'index']);

//});

