<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenjualanCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('penjualans', function (Blueprint $collection) {
            $collection->string('kendaraan_type');
            $collection->string('kendaraan_id');
            $collection->integer('jumlah_terjual');
            $collection->decimal('harga_penjualan', 8, 2);
            $collection->timestamp('tanggal_penjualan');
            $collection->string('jenis_kendaraan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->dropIfExists('penjualans');
    }
}
