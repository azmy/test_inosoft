<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::collection('users')->insert([
            'name' => 'azmy',
            'email' => 'azmysky27@gmail.com',
            'password' => bcrypt('123')
        ]);
    }
}
