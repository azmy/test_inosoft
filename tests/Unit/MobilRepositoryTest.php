<?php

namespace Tests\Unit\Repositories;

use Tests\TestCase;
use App\Models\Mobil;
use App\Repositories\MobilRepository;

class MobilRepositoryTest extends TestCase
{
    /**
     * @var MobilRepository
     */
    private $mobilRepository;

    protected function setUp(): void
    {
        parent::setUp();

        // Inisialisasi repository
        $this->mobilRepository = new MobilRepository(new Mobil());
    }

    public function testGetAll()
    {

        $data = [
            'mesin' => '1800cc',
            'kapasitas_penumpang' => 5,
            'type' => 'Sedan',
        ];

         
         $result = $this->mobilRepository->getAll();
         $this->assertEquals($data, $result);
    }

    

    public function testCreate()
    {
       
        // Mock data
        $data = [
            'mesin' => '1800cc',
            'kapasitas_penumpang' => 5,
            'type' => 'Sedan',
        ];

        // Simulate storing data to database
        $result = $this->mobilRepository->create($data);

        $this->assertInstanceOf(Mobil::class, $result);
        $this->assertEquals($data, $result->toArray());
       
    }

    /**
     * @test
     */
    
}
