<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Repositories\KendaraanRepository;
use App\Models\Kendaraan;
use Illuminate\Support\Carbon;

class KendaraanRepositoryTest extends TestCase
{
    private $kendaraanRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->kendaraanRepository = new KendaraanRepository(new Kendaraan());
    }

    public function testGetAll()
    {
        $data = [
            [
            '_id' => '647c2706405655fc9b068ed2',
            'tahun_keluaran' => 2022,
            'warna' => 'Merah',
            'stok' => 10,
            'harga' => 150000000,
            'created_at' => '2023-06-04T05:54:14.512000Z',
            'updated_at' => '2023-06-04T05:54:14.512000Z'
            ]
        ];
         
         $result = $this->kendaraanRepository->getAll();
         $this->assertEquals($data, $result);
    }

    

    public function testCreate()
    {
        // Mock data
        $data = [
            'tahun_keluaran' => 2022,
            'warna' => 'Merah',
            'stok' => 10,
            'harga' => 150000000,
        ];

        // Simulate storing data to database
        $result = $this->kendaraanRepository->create($data);

        $this->assertInstanceOf(Kendaraan::class, $result);
        $this->assertEquals($data, $result);
       
    }
}
