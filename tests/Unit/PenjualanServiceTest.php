<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Services\PenjualanService;
use App\Repositories\PenjualanRepository;
use App\Models\Penjualan;

class PenjualanServiceTest extends TestCase
{
    private $penjualanService;
    private $penjualanRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->penjualanRepository = $this->createMock(PenjualanRepository::class);
        $this->penjualanService = new PenjualanService($this->penjualanRepository);
    }

    public function testGetAllPenjualan()
    {
        $penjualan1 = new Penjualan(['kendaraan_type' => 'Mobil', 'kendaraan_id' => '647c2706405655fc9b068ed2', 'jumlah_terjual' => 2, 'harga_penjualan' => 100000000, 'tanggal_penjualan' => now()]);
        $penjualan2 = new Penjualan(['kendaraan_type' => 'Motor', 'kendaraan_id' => '2', 'jumlah_terjual' => 1, 'harga_penjualan' => 50000000, 'tanggal_penjualan' => now()]);

        $this->penjualanRepository->expects($this->once())
            ->method('getAll')
            ->willReturn([$penjualan1, $penjualan2]);

        $result = $this->penjualanService->getAllPenjualan();

        $this->assertEquals(2, count($result));
        $this->assertInstanceOf(Penjualan::class, $result[0]);
        $this->assertInstanceOf(Penjualan::class, $result[1]);
    }

    public function testCreatePenjualan()
    {
        $data = [
            'kendaraan_type' => 'Mobil',
            'kendaraan_id' => '647c2706405655fc9b068ed2',
            'jumlah_terjual' => 2,
            'harga_penjualan' => 100000000,
            'tanggal_penjualan' => now(),
        ];

        $this->penjualanRepository->expects($this->once())
            ->method('create')
            ->willReturn(true);

        $result = $this->penjualanService->createPenjualan($data);

        $this->assertTrue($result);
    }
}
