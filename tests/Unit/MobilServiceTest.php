<?php

use App\Repositories\MobilRepository;
use App\Services\MobilService;
use PHPUnit\Framework\TestCase;

class MobilServiceTest extends TestCase
{
    public function testConstructor()
    {
        $repository = $this->createMock(MobilRepository::class);

        $service = new MobilService($repository);

        $this->assertInstanceOf(MobilService::class, $service);
    }
}
