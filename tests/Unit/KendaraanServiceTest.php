<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\KendaraanService;
use App\Repositories\KendaraanRepository;
use App\Models\Kendaraan;

class KendaraanServiceTest extends TestCase
{
    private $kendaraanService;
    private $kendaraanRepository;

    protected function setUp(): void
    {
        parent::setUp();

        // Membuat mock untuk KendaraanRepository
        $this->kendaraanRepository = $this->createMock(KendaraanRepository::class);

        // Membuat instance KendaraanService dengan menggunakan mock KendaraanRepository
        $this->kendaraanService = new KendaraanService($this->kendaraanRepository);
    }

    public function testGetKendaraanById()
    {
        $id = 1;

        // Mock hasil yang diharapkan dari KendaraanRepository
        $expectedResult = new Kendaraan();
        $expectedResult->id = $id;
        $expectedResult->tahun_keluaran = 2022;
        $expectedResult->warna = 'Merah';
        $expectedResult->stok = 10;
        $expectedResult->harga = 150000000;

        // Mengatur mock KendaraanRepository untuk mengembalikan hasil yang diharapkan
        $this->kendaraanRepository->expects($this->once())
            ->method('getById')
            ->with($id)
            ->willReturn($expectedResult);

        // Memanggil metode getKendaraanById pada KendaraanService
        $result = $this->kendaraanService->getKendaraanById($id);

        // Memeriksa apakah hasil yang dikembalikan sesuai dengan hasil yang diharapkan
        $this->assertEquals($expectedResult, $result);
    }

    public function testCreateKendaraan()
    {
        // Mock data
        $data = [
            'tahun_keluaran' => 2022,
            'warna' => 'Merah',
            'stok' => 10,
            'harga' => 150000000,
        ];

        // Mock objek Kendaraan yang akan dibuat oleh KendaraanRepository
        $createdKendaraan = new Kendaraan();
        $createdKendaraan->id = 1;
        $createdKendaraan->tahun_keluaran = $data['tahun_keluaran'];
        $createdKendaraan->warna = $data['warna'];
        $createdKendaraan->stok = $data['stok'];
        $createdKendaraan->harga = $data['harga'];

        // Mengatur mock KendaraanRepository untuk mengembalikan objek Kendaraan yang dibuat
        $this->kendaraanRepository->expects($this->once())
            ->method('create')
            ->with($data)
            ->willReturn($createdKendaraan);

        // Memanggil metode createKendaraan pada KendaraanService
        $result = $this->kendaraanService->createKendaraan($data);

        // Memeriksa apakah hasil yang dikembalikan sesuai dengan objek Kendaraan yang dibuat
        $this->assertEquals($createdKendaraan, $result);
    }

    // Metode lainnya (updateKendaraan dan deleteKendaraan) dapat diuji dengan pend
}