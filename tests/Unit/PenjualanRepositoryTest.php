<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Repositories\PenjualanRepository;
use App\Models\Penjualan;
use Illuminate\Support\Carbon;

class PenjualanRepositoryTest extends TestCase
{
    private $penjualanRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->penjualanRepository = new PenjualanRepository();
    }

    public function testGetAll()
    {
        // Mock data
        $penjualan =array([
                "_id" => "647b10a8250c4c765a032382",
                "kendaraan_type"=>"Mobil",
                "kendaraan_id"=>"647c2706405655fc9b068ed2",
                "jumlah_terjual"=>2,
                "harga_penjualan"=> 100000000,
                "tanggal_penjualan" => "2023-06-03 10:06:32",
                "updated_at" => "2023-06-03T10:06:32.556000Z",
                "created_at" => "2023-06-03T10:06:32.556000Z",
        ],
        [
            "_id"=> "647bf8e943914f1ee3599555",
            "kendaraan_type"=>"Mobil",
            "kendaraan_id"=>"1",
            "jumlah_terjual"=>2,
            "harga_penjualan"=>100000000,
            "tanggal_penjualan"=>"2023-06-03 10:06:32",
            "updated_at" => "2023-06-03T10:06:32.556000Z",
            "created_at" => "2023-06-03T10:06:32.556000Z",
         ]);

       

        // Simulate fetching data from database
        $data = $penjualan;
        //dd($data);
        $result = $this->penjualanRepository->getAll();
        $this->assertEquals($data, $result);
    }

    

    public function testCreate()
    {
        // Mock data
        $data = [
            'kendaraan_type' => 'Mobil',
            'kendaraan_id' => '647c2706405655fc9b068ed2',
            'jumlah_terjual' => 2,
            'harga_penjualan' => 100000000,
            'tanggal_penjualan' => now()->toDateTimeString(),
        ];

        // Simulate storing data to database
        $result = $this->penjualanRepository->create($data);

        $this->assertInstanceOf(Penjualan::class, $result);
        $this->assertEquals('Mobil', $result['kendaraan_type']);
        $this->assertEquals('1', $result['kendaraan_id']);
        $this->assertEquals(2, $result['jumlah_terjual']);
        $this->assertEquals(100000000, $result['harga_penjualan']);
        $this->assertEquals(now()->toDateTimeString(), $result['tanggal_penjualan']);
    }
}
