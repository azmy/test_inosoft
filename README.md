Persyaratan Sistem
Sebelum memulai instalasi, pastikan sistem Anda memenuhi persyaratan berikut:

- PHP versi 8.1 atau yang lebih baru
- Composer (untuk mengelola dependensi PHP)
- MongoDB versi 4.2 atau yang lebih baru

Langkah 1: Menyiapkan Repositori
1 . Clone repositori ini ke direktori lokal Anda:
    git clone <https://gitlab.com/azmy/test_inosoft.git>
2.  Pindah ke direktori proyek yang baru dibuat:
    cd <NAMA_PROYEK>
Langkah 2: Menginstal Dependensi
1. Pastikan Anda sudah menginstal Composer. Jika belum, ikuti panduan resmi Composer untuk   menginstalnya.

2. Jalankan perintah berikut untuk menginstal semua dependensi PHP:
   composer install

Langkah 3: Konfigurasi Database
1. Salin file .env.example dan ubah namanya menjadi .env:
  cp .env.example .env

2. Buka file .env menggunakan editor teks dan atur pengaturan koneksi database MongoDB Anda, termasuk DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, dan DB_PASSWORD.

3. Simpan file .env setelah mengonfigurasi pengaturan database.

Langkah 4: Membuat Kunci Aplikasi
1. Jalankan perintah berikut untuk menghasilkan kunci aplikasi:
php artisan key:generate

2. Kunci aplikasi baru akan ditambahkan ke file .env Anda.

Berikut adalah panduan instalasi untuk API penjualan perkendaan, API stok kendaraan, dan laporan penjualan kendaraan dengan menggunakan Laravel dan MongoDB. Ikuti langkah-langkah di bawah ini untuk mengatur proyek Anda:

Persyaratan Sistem
Sebelum memulai instalasi, pastikan sistem Anda memenuhi persyaratan berikut:

PHP versi 7.4 atau yang lebih baru
Composer (untuk mengelola dependensi PHP)
MongoDB versi 4.0 atau yang lebih baru
Langkah 1: Menyiapkan Repositori
Clone repositori ini ke direktori lokal Anda:

bash
Copy code
git clone <URL_REPOSITORI>
Pindah ke direktori proyek yang baru dibuat:

bash
Copy code
cd <NAMA_PROYEK>
Langkah 2: Menginstal Dependensi
Pastikan Anda sudah menginstal Composer. Jika belum, ikuti panduan resmi Composer untuk menginstalnya.

Jalankan perintah berikut untuk menginstal semua dependensi PHP:

Copy code
composer install
Langkah 3: Konfigurasi Database
Salin file .env.example dan ubah namanya menjadi .env:

bash
Copy code
cp .env.example .env
Buka file .env menggunakan editor teks dan atur pengaturan koneksi database MongoDB Anda, termasuk DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, dan DB_PASSWORD.

Simpan file .env setelah mengonfigurasi pengaturan database.

Langkah 4: Membuat Kunci Aplikasi
Jalankan perintah berikut untuk menghasilkan kunci aplikasi:

vbnet
Copy code
php artisan key:generate
Kunci aplikasi baru akan ditambahkan ke file .env Anda.

Langkah 5: Menjalankan Migrasi Database
1. Jalankan perintah berikut untuk menjalankan migrasi database:
php artisan migrate

Langkah 6: Menjalankan Seeder (Opsional)
Jika Anda ingin mengisi database dengan data awal yang sudah ada, Anda dapat menjalankan seeder. Seeder akan menambahkan data awal ke tabel yang telah ditentukan.

1. Jalankan perintah berikut untuk menjalankan seeder
php artisan db:seed

Langkah 7: Menjalankan Server Pengembangan
1.Terakhir, jalankan perintah berikut untuk memulai server pengembangan Laravel:
php artisan serve
2.Server pengembangan akan berjalan di http://localhost:8000/ atau di port lain yang ditampilkan di konsol.

Selamat! Anda telah berhasil menginstal dan menjalankan API penjualan perkendaan, API stok kendaraan, dan laporan penjualan kendaraan menggunakan Laravel 8 dan MongoDB 4.2. Anda dapat mengakses API melalui URL yang sesuai dengan route yang didefinisikan di proyek Anda. Pastikan untuk membaca dokumentasi API yang ada untuk memahami route yang tersedia dan cara menggunakannya.

Note:
 autentikasi (Json Web Token) Belum Sempat di selesaikan.

