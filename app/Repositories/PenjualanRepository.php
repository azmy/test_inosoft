<?php

namespace App\Repositories;

use App\Models\Penjualan;
use MongoDB\BSON\ObjectId;

class PenjualanRepository
{
    public function create($data)
    {
        return Penjualan::create($data);
    }

    public function getById($id)
    {
        return Penjualan::find($id);
    }

    public function getAll()
    {
        return Penjualan::all()->toArray();
           
    }

    public function whereHas($id){
      
        // return Penjualan::whereHas('kendaraan', function ($query) use ($id) {
        //         $query->where(array('kendaraan_id'=> $id));
        // })->get();

        return Penjualan::with('kendaraan')->where(array('kendaraan_id'=> $id))->get();
    }


    public function update($id, $data)
    {
        $penjualan = Penjualan::find($id);
        if ($penjualan) {
            $penjualan->update($data);
            return $penjualan;
        }
        return null;
    }

    public function delete($id)
    {
        $penjualan = Penjualan::find($id);
        if ($penjualan) {
            $penjualan->delete();
            return true;
        }
        return false;
    }
}
