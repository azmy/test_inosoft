<?php

namespace App\Repositories;

use App\Models\Mobil;

class MobilRepository extends KendaraanRepository
{
    public function __construct(Mobil $model)
    {
        parent::__construct($model);
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getAll()
    {
        return Mobil::all()->toArray();
           
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        $kendaraan = $this->model->findOrFail($id);
        $kendaraan->update($data);
        return $kendaraan;
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }
}
