<?php

namespace App\Repositories;

use App\Models\Motor;

class MotorRepository extends KendaraanRepository
{
    public function __construct(Motor $model)
    {
        parent::__construct($model);
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getAll()
    {
        return Motor::all()->toArray();
           
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        $kendaraan = $this->model->findOrFail($id);
        $kendaraan->update($data);
        return $kendaraan;
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }
}
