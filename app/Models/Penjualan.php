<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Penjualan extends Model
{
    use HasFactory;

    protected $collection = 'penjualans';

    protected $fillable = [
        'kendaraan_id',
        'jumlah_terjual',
        'harga_penjualan',
        'tanggal_penjualan',
        'kendaraan_type', // Properti tambahan untuk jenis kendaraan (misal: 'motor' atau 'mobil')
    ];

    public function kendaraan()
    {
        return $this->belongsTo(Kendaraan::class, 'kendaraan_id');
    }

}
