<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Mobil extends Kendaraan
{
    use HasFactory;
    protected $collection = 'mobils';

    protected $fillable = [
        'mesin',
        'kapasitas_penumpang',
        'type',
        'kendaraan_id'
    ];

    public function kendaraan()
    {
        return $this->morphTo();
    }
    
}
