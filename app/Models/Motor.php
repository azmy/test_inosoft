<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Motor extends Kendaraan
{
    use HasFactory;
    protected $collection = 'motors';

    protected $fillable = [
        'mesin',
        'type_suspensi',
        'type_transmisi',
        'kendaraan_id'
    ];
}
