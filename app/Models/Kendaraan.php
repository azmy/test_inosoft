<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Kendaraan extends Model
{
    use HasFactory;
    protected $collection = 'kendaraans';
    
    protected $fillable = [
        'tahun_keluaran',
        'warna',
        'stok',
        'harga',
    ];

    
    public function mobils()
    {
        return $this->embedsOne(Mobil::class);
    }


}
