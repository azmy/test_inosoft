<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\PenjualanRepository;
use App\Services\PenjualanService;
use App\Models\Kendaraan;
use App\Models\Mobil;
use App\Models\Motor;
use App\Repositories\KendaraanRepository;
use App\Services\KendaraanService;
use App\Repositories\MotorRepository;
use App\Services\MotorService;
use App\Repositories\MobilRepository;
use App\Services\MobilService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PenjualanRepository::class, function ($app) {
            return new PenjualanRepository();
        });
    
        $this->app->bind(PenjualanService::class, function ($app) {
            return new PenjualanService($app->make(PenjualanRepository::class));
        });

        $this->app->bind(KendaraanRepository::class, function ($app) {
            return new KendaraanRepository($app->make(Kendaraan::class));
        });
    
        $this->app->bind(KendaraanService::class, function ($app) {
            return new KendaraanService($app->make(KendaraanRepository::class));
        });

        $this->app->bind(MotorRepository::class, function ($app) {
            return new MotorRepository($app->make(Motor::class));
        });
    
        $this->app->bind(MotorService::class, function ($app) {
            return new MotorService($app->make(MotorRepository::class));
        });

        $this->app->bind(MobilRepository::class, function ($app) {
            return new MobilRepository($app->make(Mobil::class));
        });
    
        $this->app->bind(MobilService::class, function ($app) {
            return new MobilService($app->make(MobilRepository::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
