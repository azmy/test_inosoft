<?php

namespace App\Services;

use App\Repositories\MobilRepository;

class MobilService extends KendaraanService
{
    public function __construct(MobilRepository $mobilRepository)
    {
        parent::__construct($mobilRepository);
    }
}
