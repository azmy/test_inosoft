<?php

namespace App\Services;

use App\Repositories\PenjualanRepository;

class PenjualanService
{
    protected $penjualanRepository;

    public function __construct(PenjualanRepository $penjualanRepository)
    {
        $this->penjualanRepository = $penjualanRepository;
    }

    public function createPenjualan($data)
    {
        return $this->penjualanRepository->create($data);
    }

    public function getAllPenjualan(){
        return $this->penjualanRepository->getAll();
    }

    public function getPenjualanById($id)
    {
        return $this->penjualanRepository->getById($id);
    }

    public function updatePenjualan($id, $data)
    {
        return $this->penjualanRepository->update($id, $data);
    }

    public function deletePenjualan($id)
    {
        return $this->penjualanRepository->delete($id);
    }

    public function whereHas($id){
        return $this->penjualanRepository->whereHas($id);
    }
}
