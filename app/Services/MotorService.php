<?php

namespace App\Services;

use App\Repositories\MotorRepository;

class MotorService extends KendaraanService
{
    public function __construct(MotorRepository $motorRepository)
    {
        parent::__construct($motorRepository);
    }
}
