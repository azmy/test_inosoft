<?php

namespace App\Http\Controllers\Api;
use App\Models\Kendaraan;
use App\Http\Controllers\Controller;
use App\Services\MotorService;
use Illuminate\Http\Request;

class MotorController extends Controller
{
    protected $motorService;

    public function __construct(MotorService $motorService)
    {
        $this->motorService = $motorService;
    }

    public function index()
    {
        $motors = $this->motorService->getKendaraanAll();
        if ($motors) {
            return response()->json([
                'status' => true,
                'data' => $motors
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function store(Request $req)
    {

        $datakendaraan = [
            'tahun_keluaran' => $req->tahun_keluaran,
            'warna' => $req->warna,
            'stok'  => $req->stok,
            'harga' => $req->harga,
       ];

        $data = [
            'mesin' => $req->mesin,
            'type_suspensi' => $req->type_suspensi,
            'type_transmisi' => $req->type_transmisi,
        ];

        $kendaraan = Kendaraan::create($datakendaraan);
        $data['kendaraan_id'] = $kendaraan->_id;

        $motor = $this->motorService->createKendaraan($data);
        if ($motor) {
            return response()->json([
                'status' => true,
                'message' => 'Data saved successfully',
                'data' => $motor
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function show($id)
    {
        $motor = $this->motorService->getKendaraanById($id);
        if ($motor) {
            return response()->json([
                'status' => true,
                'data' => $motor
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function update(Request $req, $id)
    {
        $motor = $this->motorService->getKendaraanById($id);
        if ($motor) {
            $data = [
                'mesin' => $req->mesin,
                'type_suspensi' => $req->type_suspensi,
                'type_transmisi' => $req->type_transmisi,
            ];
            $this->motorService->updateKendaraan($id, $data);
             return response()->json([
                    'status' => true,
                    'message' => 'Data updated successfully',
                    'data' => $motor
                ],201);
            } else {
                 return response()->json([
                    'status' => false,
                    'data' => 'Null'
                ],404);
            }
    }

    public function destroy($id)
    {
        $motor = $this->motorService->getKendaraanById($id);
        if ($motor) {
            $this->motorService->deleteKendaraan($motor);
            return response()->json(['status'=>true,'message' => 'Motor deleted']);
        }
        return response()->json(['message' => 'Motor not found'], 404);
    }
}
