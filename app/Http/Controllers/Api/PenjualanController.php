<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Services\PenjualanService;
use App\Services\KendaraanService;

use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    protected $penjualanService;
    protected $kendaraanService;

    public function __construct(PenjualanService $penjualanService, KendaraanService $kendaraanService)
    {
        $this->penjualanService = $penjualanService;
        $this->kendaraanService = $kendaraanService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->penjualanService->getAllPenjualan();
        if ($data) {
            return response()->json([
                'status' => true,
                'data' => $data
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    public function store(Request $req)
    {
        
        $kendaraan = $this->kendaraanService->getKendaraanById($req->kendaraan_id);
        if (empty($kendaraan)) {
            return response()->json([
                'status' => false,
                'message' => 'Kendaraan not found.',
            ],404);
        }

        if ($kendaraan->stok < $req->jumlah_terjual) {
            return response()->json([
                'status' => false,
                'message' => 'Insufficient stock.',
            ],201);
        }

        $stok = $kendaraan->stok - $req->jumlah_terjual;
        $data = [
            'stok'  => $stok
        ];
        $kendaraan = $this->kendaraanService->updateKendaraan($req->kendaraan_id, $data);

        $data = [
                 'kendaraaan_type'      => $req->kendaraan_type,
                 'kendaraan_id'         => $req->kendaraan_id,
                 'jumlah_terjual'       => $req->jumlah_terjual,
                 'harga_penjualan'      => $req->harga_penjualan,
                 'tanggal_penjualan'    => $req->tanggal_penjualan
                ];
        $penjualan = $this->penjualanService->createPenjualan($data);

        return response()->json([
            'status' => true,
            'data' => $penjualan,
        ],201);
    }

    
}
