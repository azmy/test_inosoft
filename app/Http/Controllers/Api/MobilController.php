<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\MobilService;
use App\Models\Kendaraan;
use Illuminate\Http\Request;

class MobilController extends Controller
{
    protected $mobilService;

    public function __construct(MobilService $mobilService)
    {
        $this->mobilService = $mobilService;
    }

    public function index()
    {
        $mobils = $this->mobilService->getKendaraanAll();
        if ($mobils) {
            return response()->json([
                'status' => true,
                'data' => $mobils
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function store(Request $req)
    {

       $datakendaraan = [
            'tahun_keluaran' => $req->tahun_keluaran,
            'warna' => $req->warna,
            'stok'  => $req->stok,
            'harga' => $req->harga,
       ];

        $data = [
            'mesin' => $req->mesin,
            'kapasitas_penumpang' => $req->kapasitas_penumpang,
            'type' => $req->type
        ];

        $kendaraan = Kendaraan::create($datakendaraan);
        $data['kendaraan_id'] = $kendaraan->_id;
        $mobil = $this->mobilService->createKendaraan($data);

        if ($mobil) {
            return response()->json([
                'status' => true,
                'message' => 'Data saved successfully',
                'data' => $mobil
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function show($id)
    {
        $mobil = $this->mobilService->getKendaraanById($id);
        if ($mobil) {
            return response()->json([
                'status' => true,
                'data' => $mobil
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function update(Request $req, $id)
    {
        $mobil = $this->mobilService->getKendaraanById($id);
        if ($mobil) {
            $data = [
                'mesin' => $req->mesin,
                'kapasitas_penumpang' => $req->kapasitas_penumpang,
                'type' => $req->type
            ];
            $this->mobilService->updateKendaraan($id, $data);
            
            return response()->json([
                    'status' => true,
                    'message' => 'Data updated successfully',
                    'data' => $mobil
                ],201);
            } else {
                 return response()->json([
                    'status' => false,
                    'data' => 'Null'
                ],404);
            }
       
    }

    public function destroy($id)
    {
        $mobil = $this->mobilService->getKendaraanById($id);
        if ($mobil) {
            $this->mobilService->deleteKendaraan($mobil);
            return response()->json(['status'=>true,'message' => 'Mobil deleted']);
        }
        return response()->json(['message' => 'Mobil not found'], 404);
    }
}
