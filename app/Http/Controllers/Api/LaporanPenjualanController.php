<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\PenjualanService;
use Illuminate\Http\Request;

class LaporanPenjualanController extends Controller
{
    protected $penjualanService;

    public function __construct(PenjualanService $penjualanService)
    {
        $this->penjualanService = $penjualanService;
    }

    public function index($id)
    {
            
            $data = $this->penjualanService->whereHas($id);
            //dd($data);
            
            if ($data) {
                return response()->json([
                    'message' => true,
                    'data' => $data
                ],201);
            } else {
                 return response()->json([
                    'status' => false,
                    'data' => 'Null'
                ],404);
            }
    }
}
