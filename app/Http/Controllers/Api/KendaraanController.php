<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\KendaraanService;
use Illuminate\Http\Request;

class KendaraanController extends Controller
{
    private $kendaraanService;

    public function __construct(KendaraanService $kendaraanService)
    {
        $this->kendaraanService = $kendaraanService;
    }

    public function index()
    {
        $kendaraans = $this->kendaraanService->getKendaraanAll();
        if ($kendaraans) {
            return response()->json([
                'status' => true,
                'data' => $kendaraans
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function store(Request $req)
    {
        $data = [
            'tahun_keluaran' => $req->tahun_keluaran,
            'warna' => $req->warna,
            'stok'  => $req->stok,
            'harga' => $req->harga
        ];
        $kendaraans = $this->kendaraanService->createKendaraan($data);
        if ($kendaraans) {
            return response()->json([
                'status' => true,
                'message' => 'Data saved successfully',
                'data' => $kendaraans
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function show($id)
    {
        $kendaraan = $this->kendaraanService->getKendaraanById($id);
        if ($kendaraan) {
            return response()->json([
                'status' => true,
                'data' => $kendaraan
            ],201);
        } else {
             return response()->json([
                'status' => false,
                'data' => 'Null'
            ],404);
        }
    }

    public function update(Request $req, $id)
    {
        $kendaraan = $this->kendaraanService->getKendaraanById($id);
        if ($kendaraan) {
            $data = [
                'tahun_keluaran' => $req->tahun_keluaran,
                'warna' => $req->warna,
                'stok'  => $req->stok,
                'harga' => $req->harga
            ];
            $this->kendaraanService->updateKendaraan($id, $data);
             return response()->json([
                    'status' => true,
                    'message' => 'Data updated successfully',
                    'data' => $kendaraan
                ],201);
            } else {
                 return response()->json([
                    'status' => false,
                    'data' => 'Null'
                ],404);
            }
    }

    public function destroy($id)
    {
        $kendaraan = $this->kendaraanService->getKendaraanById($id);
        if ($kendaraan) {
            $this->kendaraanService->deleteKendaraan($kendaraan);
            return response()->json(['status'=>true,'message' => 'Kendaraan deleted']);
        }
        return response()->json(['message' => 'Kendaraan not found'], 404);
    }
}
